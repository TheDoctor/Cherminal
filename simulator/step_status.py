from enum import Enum


class StepStatus(Enum):
    COMPLETED = 0
    INVALID_MOVE = 1
    HIGHLIGHT_POS_MOVES = 2
    DO_NOTHING = 3
    KING_KILLED = 4
    PEASANT_RUN = 5
    DRAW = 6
