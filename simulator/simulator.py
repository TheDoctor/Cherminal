from os import system

import getch

from color.color import Color
from settings import Settings
from simulator.board import Board
from simulator.field import Field
from simulator.step_status import StepStatus


class Simulator:

    def __init__(self):
        self.board = Board()

    # Main game loop
    def simulate(self):
        current_step = 0
        current_player = 'B'
        step_status = StepStatus.COMPLETED

        while True:
            if step_status == StepStatus.COMPLETED:
                current_step += 1
                current_player = 'A' if current_player == 'B' else 'B'

            self.clear_screen()

            # Player A
            player_text = "Player A (White)" + (" [In Turn]" if current_player == 'A' else "")
            self.print_mid_text(player_text, style="bold", end="\n\n")

            # Board
            self.board.print_board()

            # Player B
            player_text = "Player B (Black)" + (" [In Turn]" if current_player == 'B' else "")
            self.print_mid_text(player_text, style="bold")

            # Killed figures
            self.print_left_right_text("Killed by A", "Killed by B", style="bold")

            self.print_left_right_text(", ".join([f.figure for f in self.board.killed_figures[0]]),
                                       ", ".join(f.figure for f in self.board.killed_figures[1]),
                                       style="bold")

            # Points
            self.print_left_right_text(" " + str(self.board.calc_points_for_player('A')) + " Points",
                                       str(self.board.calc_points_for_player('B')) + " Points ",
                                       style="bold")

            # Check if player has possible moves
            if not self.board.can_player_move(current_player):
                step_status = StepStatus.DRAW

            # Status text
            status_text = ""
            if step_status == StepStatus.INVALID_MOVE:
                status_text = "Invalid Move"
            elif step_status == StepStatus.HIGHLIGHT_POS_MOVES:
                status_text = "Highlight Moves"
            elif step_status == StepStatus.DRAW:
                status_text = "[#] This looks like a draw [#]"
            elif step_status == StepStatus.KING_KILLED:
                status_text = "[*] Player " + current_player + " has won [*]"
            elif step_status == StepStatus.PEASANT_RUN:
                status_text = "Switch Peasant Figure"

            print()
            self.print_mid_text(("- " + status_text + " -") if status_text != "" else "", style="bold")

            # Won -> end game
            if step_status == StepStatus.KING_KILLED:
                exit(0)

            # Draw -> end game
            if step_status == StepStatus.DRAW:
                exit(0)

            # Peasant moved to end?
            if step_status == StepStatus.PEASANT_RUN:
                Color.print("[*] Player " + current_player + " choose a new figure: ", style="bold", end="")
                new_f_name = input("").upper()

                # Is valid figure?
                if new_f_name != Settings.FIGURE_KING and self.is_valid_figure(new_f_name):
                    field_name = user_input.split(":")[1]
                    self.board.set_field_by_name(field_name,
                                                 Field(new_f_name, self.board.get_field_by_name(field_name).downwards))
                    step_status = StepStatus.DO_NOTHING
                else:
                    self.print_mid_text("[!] Invalid figure name!", style="bold", end="")
                    input("")

                # Restart game loop
                continue

            step_status = StepStatus.INVALID_MOVE

            user_input = None
            Color.print("\nMove: ", style="bold", end="")
            user_input = input("").upper()

            # Do nothing
            if user_input == "":
                step_status = StepStatus.DO_NOTHING

            # Exit
            if user_input == "#" or user_input == "Q":
                self.exit_game()

            # Help
            if user_input == "?" or user_input == "HELP":
                self.print_help()
                input("")

                step_status = StepStatus.DO_NOTHING
                continue

            # Highlight possible moves
            if "?" in user_input:
                if self.is_valid_field(user_input[:2]):
                    self.board.highlight_pos_moves(user_input[:2])
                step_status = StepStatus.HIGHLIGHT_POS_MOVES

            # Movement
            elif ":" in user_input:
                if self.is_valid_move(user_input):
                    step_status = self.board.move(user_input, current_player)

    def exit_game(self):
        print()
        self.print_mid_text("- Bye! -", style="bold")
        exit(0)

    def print_help(self):
        print()
        self.print_left_mid_right_text("+", " HELP ", "+", deco=" -", style="bold")
        self.print_surrounded_left_right_text("D2:D4", "Moves figure from D2 to D4", surround="| ", style="bold")
        self.print_surrounded_left_right_text("D4?", "Shows possible moves for figure on D4", surround="| ", style="bold")
        self.print_surrounded_left_right_text("# or STRG+C", "Exits game", surround="| ", style="bold")
        self.print_surrounded_left_right_text("? or HELP", "Prints this text", surround="| ", style="bold")
        self.print_left_mid_right_text("+", "  ", "+", deco=" -", style="bold")

    def is_valid_figure(self, f_name):
        return f_name == Settings.FIGURE_PEASANT \
               or f_name == Settings.FIGURE_KING \
               or f_name == Settings.FIGURE_QUEEN \
               or f_name == Settings.FIGURE_RUNNER \
               or f_name == Settings.FIGURE_HORSE \
               or f_name == Settings.FIGURE_TOWER

    def calc_line_width(self):
        return self.board.BOARD_SIZE * (self.board.FIELD_WIDTH + 1) + self.board.BOARD_SIZE - 2 + 2 * (
                    self.board.NUM_FIELD_PAD * 2 + 3)

    def print_surrounded_left_right_text(self, text_left, text_right, fg_col=None, bg_col=None, style=None, surround="",
                                         end="\n"):
        self.print_left_right_text(surround + text_left, text_right + surround[::-1],
                                   fg_col=fg_col, bg_col=bg_col, style=style, end=end)

    def print_left_mid_right_text(self, text_left, text_mid, text_right, deco=" ", fg_col=None, bg_col=None, style=None,
                                  end="\n"):
        Color.print(text_left, fg_col=fg_col, bg_col=bg_col, style=style, end="")
        self.print_mid_text(text_mid, fg_col=fg_col, bg_col=bg_col, style=style, deco=deco,
                            offset=(len(text_left) + len(text_right)),  end="")
        Color.print(text_right, fg_col=fg_col, bg_col=bg_col, style=style, end=end)

    def print_left_right_text(self, text_left, text_right, fg_col=None, bg_col=None, style=None, end="\n"):
        Color.print(text_left, fg_col=fg_col, bg_col=bg_col, style=style, end="")
        self.print_right_text(text_right, fg_col=fg_col, bg_col=bg_col, style=style, offset=len(text_left), end=end)

    def print_right_text(self, text, fg_col=None, bg_col=None, style=None, offset=0, end="\n"):
        text_width = self.calc_line_width()
        Color.print(" " * (text_width - len(text) - offset), end="")
        Color.print(text, fg_col=fg_col, bg_col=bg_col, style=style, end=end)

    def print_mid_text(self, text, fg_col=None, bg_col=None, style=None, deco=" ", offset=0, end="\n"):
        text_width = self.calc_line_width() - offset
        text_deco_times = int((text_width - len(text)) / len(deco) / 2)

        Color.print(text_deco_times * deco + text + deco[::-1] * text_deco_times,
                    fg_col=fg_col, bg_col=bg_col, style=style, end=end)

    def is_valid_field(self, f):
        try:
            return 64 < ord(f[0]) < 73 and 0 < int(f[1]) < 9
        except:
            return False

    def is_valid_highlight(self, highlight):
        return

    def is_valid_move(self, mov):
        try:
            fields = mov.split(":")
            for f in fields:
                if not self.is_valid_field(f):
                    return False
            return True
        except:
            return False

    def get_input(self):
        c = getch.getch()

        # Exit
        if ord(c) == 3:
            return "#"

        # Arrow keys
        if ord(c) == 27:
            getch.getch()
            arrow = ord(getch.getch())

            if arrow == 65:
                return "w"
            if arrow == 66:
                return "s"
            if arrow == 67:
                return "d"
            if arrow == 68:
                return "a"
        return c

    def clear_screen(self):
        system("clear")
