from color.color import Color
from settings import Settings
from simulator.field import Field
from simulator.step_status import StepStatus


class Board:
    # Settings
    FIELD_WIDTH = 8
    NUM_FIELD_PAD = 2
    FIELD_PAD = 2
    BOARD_SIZE = 8

    def __init__(self):
        self.board = [[Field() for _ in range(self.BOARD_SIZE)] for _ in range(self.BOARD_SIZE)]

        # Killed figures
        self.killed_figures = [[], []]

        """
        # Peasants
        for i in range(self.BOARD_SIZE):
            self.board[1][i] = Field(Settings.FIGURE_PEASANT, True)
            self.board[self.BOARD_SIZE - 2][i] = Field(Settings.FIGURE_PEASANT, False)

        # Towers
        for i in range(2):
            self.board[0][(self.BOARD_SIZE - 1 + i) % self.BOARD_SIZE] = Field(Settings.FIGURE_TOWER, True)
            self.board[self.BOARD_SIZE - 1][(self.BOARD_SIZE - 1 + i) % self.BOARD_SIZE] = Field(Settings.FIGURE_TOWER,
                                                                                                 False)

        # Knights
        for i in range(2):
            self.board[0][(self.BOARD_SIZE - 2 + i * 3) % self.BOARD_SIZE] = Field(Settings.FIGURE_HORSE, True)
            self.board[self.BOARD_SIZE - 1][(self.BOARD_SIZE - 2 + i * 3) % self.BOARD_SIZE] = Field(
                Settings.FIGURE_HORSE, False)

        # Runners
        for i in range(2):
            self.board[0][(self.BOARD_SIZE - 3 + i * 5) % self.BOARD_SIZE] = Field(Settings.FIGURE_RUNNER, True)
            self.board[self.BOARD_SIZE - 1][(self.BOARD_SIZE - 3 + i * 5) % self.BOARD_SIZE] = Field(
                Settings.FIGURE_RUNNER, False)

        # King
        self.board[0][3] = Field(Settings.FIGURE_KING, True)
        """
        self.board[self.BOARD_SIZE - 1][3] = Field(Settings.FIGURE_KING, False)

        # Queen
        self.board[0][4] = Field(Settings.FIGURE_QUEEN, True)
        #self.board[self.BOARD_SIZE - 1][4] = Field(Settings.FIGURE_QUEEN, False)

    def is_empty(self, l, c):
        return self.board[l][c].is_empty()

    def set_field(self, l, c, f):
        self.board[l][c] = f

    def to_coords(self, name):
        c = ord(name[0]) - 65
        l = int(name[1]) - 1

        return l, c

    def get_field_by_name(self, name):
        coords = self.to_coords(name)
        return self.board[coords[0]][coords[1]]

    def set_field_by_name(self, name, f):
        coords = self.to_coords(name)
        self.board[coords[0]][coords[1]] = f

    def is_ally_field(self, f, name):
        return f.downwards == self.get_field_by_name(name).downwards

    def belongs_to_player_by_coords(self, l, c, player):
        return self.belongs_to_player_by_field(self.board[l][c], player)

    def belongs_to_player_by_field(self, f, player):
        if f.is_empty():
            return False

        return f.downwards if player == 'A' else not f.downwards

    def can_player_move(self, player):
        for l in range(self.BOARD_SIZE):
            for c in range(self.BOARD_SIZE):
                if self.belongs_to_player_by_coords(l, c, player) and (len(self.get_pos_fields(l, c)) > 0):
                    return True
        return False

    def move(self, mov, player):
        field_from, field_to = mov.split(":")
        coords = self.to_coords(field_from)
        coords_to = self.to_coords(field_to)

        # Is empty?
        if self.is_empty(coords[0], coords[1]):
            return StepStatus.INVALID_MOVE

        f = self.board[coords[0]][coords[1]]
        f_to = self.board[coords_to[0]][coords_to[1]]

        # Belongs to player?
        if self.belongs_to_player_by_field(f_to, player):
            return StepStatus.INVALID_MOVE

        # Is valid?
        pos_moves = self.get_pos_fields(coords[0], coords[1])
        if field_to not in pos_moves:
            return StepStatus.INVALID_MOVE

        to_f = self.get_field_by_name(field_to)

        # Move figure
        self.set_field_by_name(field_to, f)
        self.set_field_by_name(field_from, Field())

        # King killed?
        if to_f.figure == Settings.FIGURE_KING:
            return StepStatus.KING_KILLED

        # Peasant moved to end?
        if f.figure == Settings.FIGURE_PEASANT and (coords_to[0] == 0 or coords_to[0] == self.BOARD_SIZE - 1):
            return StepStatus.PEASANT_RUN

        # Add to killed
        if to_f.figure != Settings.FIGURE_EMPTY:
            self.killed_figures[0 if player == 'A' else 1].append(to_f)

        # Set figure moved
        f.moved = True

        return StepStatus.COMPLETED

    def calc_points_for_player(self, player):
        sum = 0
        for f in self.killed_figures[0 if player == 'A' else 1]:
            sum += f.value
        return sum

    def highlight_pos_moves(self, f):
        coords = self.to_coords(f)
        f = self.board[coords[0]][coords[1]]

        pos_moves = self.get_pos_fields(coords[0], coords[1])

        for pm in pos_moves:
            self.get_field_by_name(pm).add_highlight()

    def is_on_board(self, l, c):
        return 0 <= c < self.BOARD_SIZE and 0 <= l < self.BOARD_SIZE

    def to_field_name(self, l, c):
        c = chr(65 + c)
        l = l + 1

        return c + str(l)

    def get_pos_fields(self, l, c):
        if self.is_empty(l, c):
            return []

        pos_moves = []
        f = self.board[l][c]
        if f.figure == Settings.FIGURE_PEASANT:
            next_l = (l + 1) if f.downwards else (l - 1)
            if self.is_on_board(next_l, c):
                pos_moves.append(self.to_field_name(next_l, c))

                # Is in beginning line? -> extra step
                if (l == 1) if f.downwards else (l == self.BOARD_SIZE - 2):
                    pos_moves.append(self.to_field_name((l + 2) if f.downwards else (l - 2), c))

                # Can attack?
                for i in range(-1, 2, 2):
                    if self.is_on_board(next_l, c + i) and not self.is_empty(next_l, c + i):
                        if self.board[next_l][c + i].downwards != f.downwards:
                            pos_moves.append(self.to_field_name(next_l, c + i))

        elif f.figure == Settings.FIGURE_TOWER or f.figure == Settings.FIGURE_QUEEN:
            # Top direction
            next_l = (l + 1) if f.downwards else (l - 1)
            while self.is_on_board(next_l, c):
                pos_moves.append(self.to_field_name(next_l, c))

                if not self.is_empty(next_l, c):
                    break

                next_l = (next_l + 1) if f.downwards else (next_l - 1)

            # Bot direction
            next_l = (l - 1) if f.downwards else (l + 1)
            while self.is_on_board(next_l, c):
                pos_moves.append(self.to_field_name(next_l, c))

                if not self.is_empty(next_l, c):
                    break

                next_l = (next_l - 1) if f.downwards else (next_l + 1)

            # Right direction
            next_c = (c + 1) if f.downwards else (c - 1)
            while self.is_on_board(l, next_c):
                pos_moves.append(self.to_field_name(l, next_c))

                if not self.is_empty(l, next_c):
                    break

                next_c = (next_c + 1) if f.downwards else (next_c - 1)

            # Left direction
            next_c = (c - 1) if f.downwards else (c + 1)
            while self.is_on_board(l, next_c):
                pos_moves.append(self.to_field_name(l, next_c))

                if not self.is_empty(l, next_c):
                    break

                next_c = (next_c - 1) if f.downwards else (next_c + 1)

        if f.figure == Settings.FIGURE_RUNNER or f.figure == Settings.FIGURE_QUEEN:
            # Top/Left direction
            next_c = (c - 1) if f.downwards else (c + 1)
            next_l = (l + 1) if f.downwards else (l - 1)
            while self.is_on_board(next_l, next_c):
                pos_moves.append(self.to_field_name(next_l, next_c))

                if not self.is_empty(next_l, next_c):
                    break

                next_c = (next_c - 1) if f.downwards else (next_c + 1)
                next_l = (next_l + 1) if f.downwards else (next_l - 1)

            # Top/Right direction
            next_c = (c + 1) if f.downwards else (c - 1)
            next_l = (l + 1) if f.downwards else (l - 1)
            while self.is_on_board(next_l, next_c):
                pos_moves.append(self.to_field_name(next_l, next_c))

                if not self.is_empty(next_l, next_c):
                    break

                next_c = (next_c + 1) if f.downwards else (next_c - 1)
                next_l = (next_l + 1) if f.downwards else (next_l - 1)

            # Bot/Right direction
            next_c = (c + 1) if f.downwards else (c - 1)
            next_l = (l - 1) if f.downwards else (l + 1)
            while self.is_on_board(next_l, next_c):
                pos_moves.append(self.to_field_name(next_l, next_c))

                if not self.is_empty(next_l, next_c):
                    break

                next_c = (next_c + 1) if f.downwards else (next_c - 1)
                next_l = (next_l - 1) if f.downwards else (next_l + 1)

            # Bot/Left direction
            next_c = (c - 1) if f.downwards else (c + 1)
            next_l = (l - 1) if f.downwards else (l + 1)
            while self.is_on_board(next_l, next_c):
                pos_moves.append(self.to_field_name(next_l, next_c))

                if not self.is_empty(next_l, next_c):
                    break

                next_c = (next_c - 1) if f.downwards else (next_c + 1)
                next_l = (next_l - 1) if f.downwards else (next_l + 1)

        elif f.figure == Settings.FIGURE_KING:
            for c_off in range(-1, 2):
                for l_off in range(-1, 2):
                    if self.is_on_board(l + l_off, c + c_off) and not (c_off == 0 and l_off == 0):
                        pos_moves.append(self.to_field_name(l + l_off, c + c_off))

        elif f.figure == Settings.FIGURE_HORSE:
            # Top/Right 1
            if self.is_on_board(l + 2, c + 1):
                pos_moves.append(self.to_field_name(l + 2, c + 1))

            # Top/Right 2
            if self.is_on_board(l + 1, c + 2):
                pos_moves.append(self.to_field_name(l + 1, c + 2))

            # Bot/Right 1
            if self.is_on_board(l - 1, c + 2):
                pos_moves.append(self.to_field_name(l - 1, c + 2))

            # Bot/Right 2
            if self.is_on_board(l - 2, c + 1):
                pos_moves.append(self.to_field_name(l - 2, c + 1))

            # Bot/Left 1
            if self.is_on_board(l - 2, c - 1):
                pos_moves.append(self.to_field_name(l - 2, c - 1))

            # Bot/Left 2
            if self.is_on_board(l - 1, c - 2):
                pos_moves.append(self.to_field_name(l - 1, c - 2))

            # Top/Left 1
            if self.is_on_board(l + 1, c - 2):
                pos_moves.append(self.to_field_name(l + 1, c - 2))

            # Top/Left 2
            if self.is_on_board(l + 2, c - 1):
                pos_moves.append(self.to_field_name(l + 2, c - 1))

        # Clear ally fields
        pos_moves = [pm for pm in pos_moves if not self.is_ally_field(f, pm)]

        return pos_moves

    def save_board(self):
        backup = [[Field(f.figure, f.downwards) for f in l] for l in self.board]
        return backup

    def load_board(self, backup):
        self.board = backup

    def print_board(self):
        # Print top num fields
        self._print_horizontal_num_line()

        for i, l in enumerate(self.board):
            # Print top line of fields
            self._print_empty_field_line(l)

            # Print line num
            self._print_line_num_field(i)

            # Print line containing values
            for f in l:
                if f.figure != Settings.FIGURE_EMPTY:
                    f_str = str(f.figure)
                    f_len = len(f_str)
                    pad_len = int((self.FIELD_WIDTH - f_len) / 2)
                    f_str = " " * pad_len + f_str + " " * pad_len

                    if pad_len * 2 + f_len != self.FIELD_WIDTH:
                        f_str += " "

                    Color.print(f_str, style="bold", fg_col=f.fg_col, bg_col=f.bg_col, end=" " * self.FIELD_PAD)
                else:
                    Color.print(" " * self.FIELD_WIDTH, style="bold", fg_col=f.fg_col, bg_col=f.bg_col,
                                end=" " * self.FIELD_PAD)

            # Print line num
            Color.print(" " * self.NUM_FIELD_PAD + str(i + 1) + " " * (self.NUM_FIELD_PAD + 1), style="bold", end="")
            print()

            # Print bottom line of fields
            self._print_empty_field_line(l)

            # Pad line
            print()

        # Print bot num fields
        self._print_horizontal_num_line()

        # Remove highlights
        for l in range(self.BOARD_SIZE):
            for c in range(self.BOARD_SIZE):
                self.board[l][c].remove_highlight()

    def _print_line_num_field(self, i):
        Color.print(" " * self.NUM_FIELD_PAD + str(i + 1) + " " * (self.NUM_FIELD_PAD + 1), style="bold", end="")

    def _print_horizontal_num_line(self):
        Color.print(" " * (self.NUM_FIELD_PAD * 2 + 2), end="")
        for i in range(self.BOARD_SIZE):
            Color.print(" " * int((self.FIELD_WIDTH - 1) / 2) + chr(65 + i) + " " * int(self.FIELD_WIDTH / 2),
                        style="bold", end=" " * self.FIELD_PAD)
        print("\n")

    def _print_empty_field_line(self, l):
        # Num field
        Color.print(" " * (self.NUM_FIELD_PAD * 2 + 2), end="")
        for f in l:
            Color.print(" " * self.FIELD_WIDTH, style="bold", fg_col=f.fg_col, bg_col=f.bg_col,
                        end=" " * self.FIELD_PAD)
        print()
