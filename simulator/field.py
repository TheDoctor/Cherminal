from settings import Settings


class Field:

    def __init__(self, figure=Settings.FIGURE_EMPTY, downwards=None, moved=False):
        self.downwards = downwards
        self.bg_col = None
        self.fg_col = None
        self.is_highlighted = False
        self.moved = moved
        self.value = 0
        self._set_figure(figure)

    def add_highlight(self):
        self.is_highlighted = True

        self.calc_colors()

    def remove_highlight(self):
        self.is_highlighted = False

        self.calc_colors()

    def is_empty(self):
        return self.figure == Settings.FIGURE_EMPTY

    def calc_colors(self):
        if self.is_highlighted:
            self.bg_col = "white"
            self.fg_col = "red"
        else:
            self.fg_col = "white" if self.downwards is not None and self.downwards else "black"

            if self.figure == Settings.FIGURE_PEASANT:
                self.bg_col = "cyan"
            elif self.figure == Settings.FIGURE_HORSE:
                self.bg_col = "blue"
            elif self.figure == Settings.FIGURE_RUNNER:
                self.bg_col = "yellow"
            elif self.figure == Settings.FIGURE_TOWER:
                self.bg_col = "green"
            elif self.figure == Settings.FIGURE_KING:
                self.bg_col = "red"
            elif self.figure == Settings.FIGURE_QUEEN:
                self.bg_col = "magenta"
            else:
                self.bg_col = None
                self.fg_col = None

    def _set_figure(self, figure):
        self.figure = figure

        # Set figure value
        if self.figure == Settings.FIGURE_PEASANT:
            self.value = 1
        elif self.figure == Settings.FIGURE_HORSE:
            self.value = 3
        elif self.figure == Settings.FIGURE_RUNNER:
            self.value = 3
        elif self.figure == Settings.FIGURE_TOWER:
            self.value = 4.5
        elif self.figure == Settings.FIGURE_QUEEN:
            self.value = 9
        elif self.figure == Settings.FIGURE_KING:
            self.value = 1337   # What could be more worth than 1337?
        elif self.figure == Settings.FIGURE_EMPTY:
            self.value = 0

        # Color
        self.calc_colors()
