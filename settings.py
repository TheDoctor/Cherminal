

class Settings:

    FIGURE_EMPTY = '-'
    FIGURE_PEASANT = 'B'
    FIGURE_TOWER = 'T'
    FIGURE_HORSE = 'H'
    FIGURE_RUNNER = 'R'
    FIGURE_KING = 'K'
    FIGURE_QUEEN = 'Q'
